Brief introduction by Mike Taylor (https://www.fairopenaccess.org/faq/#open)
========

Everyone agrees that at a minimum, open access means that the articles are free to read to anyone anywhere, without any technical barriers such as requiring registration.

Beyond that, there are different definitions. The three classic declarations on Open Access (Budapest, Berlin, Bethesda) all agree that OA also includes the right to re-use content: for example, the Budapest statement says “free availability on the public internet, permitting any users to read, download, copy, distribute, print, search, or link to the full texts of these articles, crawl them for indexing, pass them as data to software, or use them for any other lawful purpose, without financial, legal, or technical barriers other than those inseparable from gaining access to the internet itself”. To achieve this, the canonical licence to use is the Creative Commons Attribution (CC By) licence.

However, some people feel anxiety about their work being used in ways they do not approve of, and especially of the possibility that others may make money from it without their getting a cut. For this reason, they may prefer a more restrictive licence such as Creative Commons Attribution-NonCommercial (CC BY-NC).

This licence prevents a large and fuzzy-bordered set of re-uses. The problem is that no-one knows exactly what is and isn’t allowed under the terms “non-commercial” — it’s a thing that can be decided only in court, and on a jurisdiction-by-jurisdiction basis. For example, it may be that CC BY-NC materials can’t be used in teaching in a university, because the university charges tuition fees — an outcome that surely is not what any scholar intends for their OA works. A court in Germany ruled that any non-personal use counts as “commercial” even when no money changes hands: under that interpretation no CC BY-NC works could ever be used in any kind of teaching.

It’s for these kinds of reasons that many people who have put the most thought into open-access licences have come down on the side of CC BY. These include respected OA publishers both commercial and non-profit (BioMed Central, PLOS, PeerJ), funding charities (Wellcome Trust, Gates Foundation) and national bodies (RCUK). As a result, journals which publish open access materials under more restrictive licences will not be acceptable venues for research funded by the Wellcome Trust, Gates Foundation, etc.

We therefore strongly recommend the use of the CC-BY licence for all open access journals. But, recognising that different groups may wish to make a different choice, we require only that an explicit statement is made about which licence is used, and strongly suggest that it be one of the widely recognised and understood Creative Commons licences.




More details by Benoit Kloeckner (https://gitlab.com/publishing-reform/discussion/issues/43)
========

0. Once a paper is published under a CC licence (e.g. CC-BY, CC-BY-NC, CC-BY-ND), a journal or publisher cannot retain exclusive diffusion rights. The very meaning of these licences is that the authors (and actually anybody) can legally put the publisher-formatted pdf file on a web page, the authors can put the publisher pdf on HaL and the arXiv, etc.
1. Having a notice at the Journal's web page that contradict the licence is a significant issue, as it works against the point of CC of making clear what is authorized and what is not -- and this is a big part of Open Access. Thus, if article are stamped e.g. "CC-BY-ND" the journal should not make false claim as "The Journal reserve for themselves the exclusive right to publish the texts as laid out in the specific format of the Journal"

   Further, a claim such as "the authors are allowed to leave their original source files in free access on their private home page on servers" is logically correct, but misleading: under CC-BY (with or without ND, NC clauses) the authors have also the right to post the formated article, and mentioning a more restricted right casts doubt.

   Having a clear licence is important to make clear what is authorized and what is not, and one should of course be ready to accept the consequences of the chosen licence. A journal should in my opinion want to encourage authors to put the formated version on the arXiv (and should even provide the final .tex file for this purpose) and their web page. Indeed, nowadays many people read papers without knowing where they were published. Seeing the Journal's name front page reminds reader what great papers you publish, it is very good publicity.

2. In CC licences, the NC provision prevents other publisher to republish and sell the papers without authorization (because this would be commercial use of the papers), but non-commercial uses such as diffusion on a freely accessible web page is authorized under CC-BY-NC.

   The ND provision prevents non-authorized diffusion of a derivative (modified formating, content, etc.)

3. To abide fully to Fair Open Access principles, articles' copyright should be hold by the author. In practice, this means that if someone wants to make e.g. a collected work of some author and sell it, one has to ask permission to the author, not the Journal. It would make sense in some cases to have it the other way round, but it is to the letter in contradiction with Fair OA. The question whether to let author retain copyright or transferring it to Journal is not obvious, but honestly I am not sure of what the precise implication of copyright transferred to a journal combined with a CC licence are.

   About a natural question: "If the author retains the copyright, what does prevent him to publish a slightly improved version of the text in another journal?" 
To the best of my knowledge, this is more an academic question than a legal one. An author could even legally republish the very same article, (and from a legal point of view, putting a paper in the arXiv is publishing); but it is academic rules that will backlash at such an author, in the very same way that if one submits a paper to several journal at the same time, as far as I know there is not much ground to sue; but the academic reputation of an infringer would suffer badly.
Also, any serious journal would refuse this, so it is also a responsibility of the other journal to assess novelty in the submitted paper. Sure, some shady journals could try making a business republishing already published articles, but since said articles are already accessible freely, I do not see what the business model would be.
As far as slight improvements are concerned, it is currently a relatively common practice (well, it depends what one means by "slight", I guess). It is a problem, but rather a problem with editorial policies of journal rather than a legal problem.

   Also note that in any case, the copyright holder can waive any restrictive clause if he or she wants to. So, an author who would have kept copyright could waive for the Journal the ND clause to enable it to publish a revised version, if so needed.